//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/map.h>
#include <gfx/font_source.h>
#include <math/size.h>

#include <fthb_text/context.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace fthb_text
    {
        struct font_face
        {
            font_face() noexcept = default;

            font_face(font_face && f) noexcept :
                handle(std::exchange(f.handle, nullptr)),
                font(f.font),
                dpi(f.dpi) {}

            ~font_face() {
                if (handle) {
                    FT_Done_Face(handle);
                }
            }

            bool has_character(u32 code) {
                return FT_Get_Char_Index(handle, FT_ULong(code)) != 0;
            }

            FT_Face handle = nullptr;
            gfx::font font;
            math::uint_size dpi;
        };

        struct font_face_stack
        {
            ordered_map<gfx::font_variant, std::vector<gfx::font_id>> fonts;
        };
    }

    namespace gfx
    {
        template <>
        class font_source<fthb_text::context>
        {
        public:
            font_source(fthb_text::context & context);

            void set_dpi(const math::uint_size & dpi) noexcept {
                _dpi = dpi;
            }

            const math::uint_size dpi() const noexcept {
                return _dpi;
            }

            const math::float_size point_scale() const noexcept {
                return _dpi / 72.0f;
            }

            gfx::font_id load(const gfx::resource & resource);

//

            fthb_text::font_face & face(gfx::font_id font_id);
            fthb_text::font_face & size_face(gfx::font font);

        private:
            fthb_text::context & _context;
            std::vector<fthb_text::font_face> _faces;
            std::vector<fthb_text::font_face_stack> _font_stacks;
            math::uint_size _dpi {72, 72};
        };
    }
}
