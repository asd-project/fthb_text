//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <container/map.h>
#include <gfx/raster_font.h>
#include <fthb_text/font_source.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        constexpr gfx::texture_format get_texture_format(gfx::raster_font_format format) {
            switch (format) {
                case gfx::raster_font_format::colored:
                    return gfx::texture_format::bgra;
                case gfx::raster_font_format::grayscale:
                    return gfx::texture_format::grayscale;
            }

            BOOST_ASSERT_MSG(false, "[fthb_text][get_texture_format] Invalid font format");
            return gfx::texture_format::grayscale;
        }

        template <class Gfx>
        struct font_atlas<fthb_text::context, Gfx>
        {
            static_assert(gfx::graphics<Gfx>);

            static constexpr uint32_t margin = 2;

            font_atlas(gfx::handle<gfx::texture<Gfx>> && texture) :
                texture(std::move(texture)) {}

            font_atlas(font_atlas &&) noexcept = default;
            font_atlas & operator = (font_atlas &&) noexcept = default;

            math::uint_point reserve_area(const math::uint_size & glyph_size) {
                auto current_size = texture->size();

                if (current_pos.x + glyph_size.x > current_size.x - margin) {
                    current_pos = {margin, current_pos.y + occupied_height + margin};
                    occupied_height = 0;
                }

                if (current_pos.y + glyph_size.y > current_size.y - margin) {
                    texture->resize({current_size.x, current_size.y + 512});
                }

                math::uint_point pos = current_pos;

                current_pos.x += glyph_size.x + margin;
                occupied_height = math::max(occupied_height, glyph_size.y);

                return pos;
            }

            gfx::handle<gfx::texture<Gfx>> texture;
            math::uint_point current_pos {margin, margin};
            u32 occupied_height = 0;
        };

        template <class Gfx>
        struct font_atlas_storage<fthb_text::context, Gfx>
        {
            static_assert(gfx::graphics<Gfx>);

            font_atlas_storage(gfx::handle<gfx::texture<Gfx>> && texture) :
                atlas(std::move(texture))
            {}

            font_atlas_storage(font_atlas_storage && texture) noexcept = default;
            font_atlas_storage & operator = (font_atlas_storage && texture) noexcept = default;

            font_atlas<fthb_text::context, Gfx> atlas;
            stable_map<glyph_id, glyph> glyphs;
        };

        template <class Gfx>
        class font_rasterizer<fthb_text::context, Gfx>
        {
            static_assert(gfx::graphics<Gfx>);

        public:
            font_rasterizer(fthb_text::context & context, gfx::font_source<fthb_text::context> & font_source, gfx::texture_registry<Gfx> & texture_registry) :
                _context(context),
                _font_source(font_source),
                _texture_registry(texture_registry)
            {}

            static auto create_texture(gfx::texture_registry<Gfx> & texture_registry, gfx::font font, raster_font_format preferred_format) {
                math::size preferred_size(font.size * 16, font.size * 16);

                gfx::texture_data<Gfx> texture_data;
                texture_data.size = math::max(math::next_power_of_two(preferred_size), {1024, 1024});
                texture_data.format = get_texture_format(preferred_format);
                texture_data.filtering = gfx::texture_filtering::linear;

                std::vector<std::byte> pixels(texture_data.size.area() * gfx::bytes_per_pixel(texture_data.format));
                texture_data.pixels = pixels;

                return texture_registry.create(std::move(texture_data));
            }

            gfx::texture<Gfx> & rasterize(const glyph_range & glyphs, const raster_font_data & font_data) {
                auto & font_face = _font_source.size_face(font_data.font);
                auto preferred_format = FT_HAS_COLOR(font_face.handle) ? font_data.preferred_format : raster_font_format::grayscale;

                auto it = _cache.find(font_data.font);

                if (it == _cache.end()) {
                    it = _cache.emplace(font_data.font, create_texture(_texture_registry, font_face.font, preferred_format)).first;
                }

                auto & storage = it.value();

                for (auto & slot : glyphs) {
                    gfx::glyph & glyph = render_glyph(slot.id, storage, font_face, preferred_format);

                    if (!glyph.local_area.empty()) {
                        slot.glyph = &glyph;
                    }
                }

                return *storage.atlas.texture;
            }

        private:
            gfx::glyph & render_glyph(glyph_id id, font_atlas_storage<fthb_text::context, Gfx> & storage, fthb_text::font_face & font_face, raster_font_format preferred_format) {
                auto it = storage.glyphs.find(id);

                if (it != storage.glyphs.end()) {
                    return it->second;
                }

                FT_Int32 flags = FT_LOAD_TARGET_NORMAL | FT_LOAD_RENDER;

                if (preferred_format != raster_font_format::grayscale) {
                    flags |= FT_LOAD_COLOR;
                }

                if (auto err = FT_Load_Glyph(font_face.handle, FT_UInt(id), flags); err != 0) {
                    BOOST_THROW_EXCEPTION(std::runtime_error("Couldn't load glyph"));
                }

                FT_GlyphSlot slot = font_face.handle->glyph;
                FT_Bitmap bitmap = slot->bitmap;

                auto & glyph = storage.glyphs.emplace(id, gfx::glyph{}).first->second;

                if (!bitmap.buffer) {
                    return glyph;
                }

                auto size = math::size(bitmap.width, bitmap.rows);
                auto pos = storage.atlas.reserve_area(size);

                glyph.local_area = {math::int_point(slot->bitmap_left, font_face.font.size * _font_source.point_scale().y - slot->bitmap_top), math::int_size(size)};
                glyph.atlas_area = {pos, size};

                storage.atlas.texture->update_region(glyph.atlas_area, pixel_data{bitmap.buffer, size.area() * storage.atlas.texture->bpp()});

                return glyph;
            }

            fthb_text::context & _context;
            gfx::font_source<fthb_text::context> & _font_source;
            gfx::texture_registry<Gfx> & _texture_registry;
            map<gfx::font, font_atlas_storage<fthb_text::context, Gfx>> _cache;
        };
    }
}
