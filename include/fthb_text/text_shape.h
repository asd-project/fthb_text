//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gfx/text_shape.h>
#include <fthb_text/context.h>
#include <fthb_text/font_source.h>

#include <hb.h>
#include <hb-ft.h>

#include <meta/concepts.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        struct shaped_font
        {
            shaped_font(fthb_text::font_face & face) {
                handle = hb_ft_font_create(face.handle, nullptr);
            }

            shaped_font(shaped_font && f) noexcept :
                handle(std::exchange(f.handle, nullptr))
            {}

            ~shaped_font() {
                if (handle != nullptr) {
                    hb_font_destroy(handle);
                }
            }

            hb_font_t * handle = nullptr;
        };

        template <>
        class text_shaper<fthb_text::context>
        {
        public:
            text_shaper(gfx::font_source<fthb_text::context> & source) noexcept : _source(source) {
                _buffer = hb_buffer_create();
            }

            ~text_shaper() {
                if (_buffer) {
                    hb_buffer_destroy(_buffer);
                }
            }

            gfx::pixel_size shape(std::vector<glyph_slot> & glyph_slots, const gfx::text_fragment & fragment, u32 size, gfx::pixel_point offset = {0, 0}) noexcept;

        private:
            gfx::font_source<fthb_text::context> & _source;
            map<gfx::font, shaped_font> _fonts;
            hb_buffer_t * _buffer;
        };
    }
}
