//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <stdexcept>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OUTLINE_H

//---------------------------------------------------------------------------

namespace asd
{
    namespace fthb_text
    {
        class context
        {
        public:
            context() {
                if (FT_Init_FreeType(&_handle) != 0) {
                    BOOST_THROW_EXCEPTION(std::runtime_error("Can't init free type context"));
                }
            }

            ~context() {
                if (_handle) {
                    FT_Done_FreeType(_handle);
                }
            }

            operator FT_Library() const noexcept {
                return _handle;
            }

        private:
            FT_Library _handle = nullptr;
        };
    }
}
