//---------------------------------------------------------------------------

#if defined(TYPOGRAPHY_USE_BOOST_TEXT)
#include <boost/text/line_break.hpp>
#include <boost/text/bidirectional.hpp>
#endif

#include <app/label.h>
#include <color/preset.h>

//---------------------------------------------------------------------------

namespace asd::app::widgets
{
    label::label(ecs::handle && model, const gfx::font & font) noexcept :
        ui::widgets::model_widget(std::move(model))
    {
        _model.patch(def::text, [&](auto & layer) {
            layer.subranges.push_back({_text, font, color::black}); 
        });
    }

    static bool can_break_after(u32 cp) {
        return cp == ' ' || cp == '\t' || cp == '\n' || cp == 0x200B;
    }

    void label::set_text(const gfx::text & text) noexcept {
        _model.patch(def::text, [&](auto & text_model) {
            if (text.empty()) {
                text_model.subranges.erase(text_model.subranges.begin() + 1, text_model.subranges.end());
                text_model.subranges[0].text = _text;
                return;
            }

            _text = text;

            auto font = text_model.subranges[0].font;
            auto color = text_model.subranges[0].color;

            text_model.bidi_subranges.clear();
            text_model.subranges.clear();

#if defined(TYPOGRAPHY_USE_BOOST_TEXT)
            for (auto word : boost::text::allowed_lines(_text)) {
                for (auto range : boost::text::bidirectional_subranges(word)) {
                    auto & subrange = text_model.subranges.emplace_back("", font, color);

                    for (auto grapheme : range) {
                        subrange.text.append(grapheme);
                    }
                }
            }
#else
            auto subrange_begin = gfx::text_begin(_text);
            auto text_end = gfx::text_end(_text);

            for (auto it = subrange_begin; it != text_end;) {
                auto cp = gfx::text_code_point(it);
                ++it;

                if (can_break_after(cp)) {
                    text_model.subranges.emplace_back(gfx::make_text_view(std::exchange(subrange_begin, it), it), font, color);
                }
            }

            text_model.subranges.emplace_back(gfx::make_text_view(subrange_begin, text_end), font, color);
#endif
        });
    }

    void label::set_font(const gfx::font & font) noexcept {
        _model.patch(def::text, [&](auto & text_model) {
            for (auto & subrange : text_model.subranges) {
                subrange.font = font;
            }
        });
    }

    void label::set_color(const color::linear_rgb & color) noexcept {
        _model.patch(def::text, [&](auto & text_model) {
            for (auto & subrange : text_model.subranges) {
                subrange.color = color;
            }
        });
    }

    void label::set_direction(text_direction direction) noexcept {
        _model.patch(def::text, [&](auto & text_model) {
            if (text_model.direction == direction) {
                return false;
            }

            text_model.direction = direction;
            return true;
        });
    }

    void label::set_alignment(text_alignment alignment) noexcept {
        _model.patch(def::text, [&](auto & text_model) {
            if (text_model.alignment == alignment) {
                return false;
            }

            text_model.alignment = alignment;
            return true;
        });
    }
}

//---------------------------------------------------------------------------
