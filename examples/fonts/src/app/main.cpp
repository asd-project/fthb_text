//---------------------------------------------------------------------------

#include <launch/main.h>

#include <math/io.h>
#include <color/preset.h>

#include <uio/shortcut.h>

#include <sdlpp/module.h>

#include <flow/run_main.h>
#include <flow/timer.h>
#include <scene/camera.h>

#include <ui/widgets/box.h>
#include <ui/widgets/container.h>
#include <ui/widgets/scroll_area.h>
#include <ui/widgets/slot.h>
#include <ui/layouts/grid.h>
#include <ui/behaviours/inertial_scrolling.h>
#include <ui/controllers/root_controller.h>
#include <ui/constraints/aspect_ratio.h>
#include <ui/systems/geometry_system.h>

#include <app/label.h>
#include <app/label_renderer.h>

#include <fthb_text/font_source.h>
#include <fthb_text/text_shape.h>
#include <fthb_text/raster_font.h>

#include <spdlog/spdlog.h>

//---------------------------------------------------------------------------

namespace asd
{
    using namespace std::chrono_literals;
    using namespace color::literals;

    constexpr color::linear_rgb clear_color = 0x2a2a2a_rgb;

    using ui_graphics = opengl::graphics;
    using ui_font_context = fthb_text::context;

    constexpr auto systems =
        app::text_render_system_type<ui_graphics, ui_font_context> +
        ui::def::geometry_system;

    struct gui
    {
        using context_type = ecs::context_of<systems>;

        gui(
            context_type & context,
            gfx::uniform_registry<ui_graphics> & uniforms,
            gfx::render_pass_registry<ui_graphics> & passes,
            gfx::font_source<ui_font_context> & font_source
        ) :
            context(context),
            camera_view(uniforms),
            render_pass(passes.create({{}, gfx::blending::interpolative, clear_color}))
        {
            font_source.set_dpi(math::uint_size(sdl::get_dpi()));

            camera.set_projection(scene::projection::screen);
            camera.set_far_distance(2.0f);
            camera.set_position({0.0f, 0.0f, -1.0f});
            camera.set_direction({0.0f, 0.0f, 1.0f});

            default_font.id = font_source.load(gfx::path_resource{application::root_path() / "fonts/Arial Unicode.ttf"});
            default_font.size = 24;
        }

        void update() {
            camera_view.update(camera);
            ecs::update(context);
        }

        void render() {
            render_pass->start();
            camera_view.activate();
            ui::render(context);
            render_pass->end();
        }

        context_type & context;
        scene::camera camera;
        scene::camera_view<opengl::graphics> camera_view;
        gfx::handle<gfx::render_pass<opengl::graphics>> render_pass;
        gfx::font default_font;
        signal::hub hub;
    };

    constexpr color::linear_rgb text_color = color::light_gray;

    auto create_view = [](asd::gui & gui, ui::routine_context & routines) {
        using namespace ui::s;

        auto make_label = [&](const gfx::text & t, app::text_direction dir = app::text_direction::automatic) {
            using ui::s::color;

            return ui::cell_slot(
                min = space::size(300, 50),

                app::label(
                    context = gui.context,
                    font = gui.default_font,
                    text = t,
                    direction = dir,
                    color = text_color
                )
            );
        };

        return ui::scroll_area(
            controller = ui::behaviour_controller(
                ui::inertial_scrolling(
                    routine_context = routines,
                    wheel_scroll_amount = 4.0f,
                    deceleration = 1000.0f
                )
            ),

            ui::container::make_static(
                layout = ui::grid(
                    spacing = space::size{16.0f, 12.0f},
                    margin = 16.0f
                ),

                make_label(
                    "Lorem ipsum dolor sit amet, mel habeo facer movet ex, pro "
                    "menandri delicatissimi ex, nec eu utinam neglegentur."
                ),
                make_label(
                    "ציור דרכה למאמרים את שמו, אל שער כלים לעתים. מתן של ליום תרבות תקשורת, ב כדי כימיה לתרום נבחרים, גם מתן ננקטת בישול אל.",
                    app::text_direction::rtl
                ),
                make_label(
                    "旅ロ京青利セムレ弱改フヨス波府かばぼ意送でぼ調掲察たス日西重ケアナ住橋ユムミク順待ふかんぼ人奨貯鏡すびそ。"
                ),
                make_label(
                    "Лорем ипсум долор сит амет, пер цлита поссит ех, ат мунере фабулас петентиум сит. "
                    "Иус цу цибо саперет сцрипсерит, нец виси муциус лабитур ид"
                ),
                make_label(
                    "लेकिन साधन गटकउसि उपलब्धता सभिसमज सम्पर्क प्रव्रुति सके। प्रति रखति भाषाओ दुनिया बेंगलूर परिभाषित जाने गटकउसि दस्तावेज सिद्धांत बाजार हुआआदी"
                ),
                make_label(
                    "국민경제의 발전을 위한 중요정책의 수립에 관하여 대통령의 자문에 응하기 위하여 국민경제자문회의를 둘 수 있다."
                )
            )
        );
    };

    struct window_container
    {
        sdl::app & app;
        sdl::graphics & graphics;
        asd::gui & ui;
        sdl::window & window;
        sdl::mouse & mouse;
        sdl::keyboard & keyboard;

        ui::routine_context routines;
        uio::shortcut_context shortcuts;

        std::unique_ptr<SDL_Cursor, void(*)(SDL_Cursor *)> arrow_cursor{SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW), SDL_FreeCursor};
        std::unique_ptr<SDL_Cursor, void(*)(SDL_Cursor *)> hand_cursor{SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND), SDL_FreeCursor};

        ui::widgets::box root{create_view(ui, routines)};
        ui::controllers::root_controller<ui::widgets::box> controller{root};

        bool fullscreen = false;

        window_container(sdl::app & app, sdl::graphics & graphics, asd::gui & ui, sdl::window & window, sdl::mouse & mouse, sdl::keyboard & keyboard) :
            app(app),
            graphics(graphics),
            ui(ui),
            window(window),
            mouse(mouse),
            keyboard(keyboard),
            shortcuts(keyboard, ui.hub)
        {
            using namespace uio::literals;

            shortcuts.subscribe("Alt+Enter"_shortcut, [this](const uio::shortcut &) {
                this->fullscreen = !this->fullscreen;

                if (this->fullscreen) {
                    SDL_SetWindowFullscreen(this->window, SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_OPENGL);
                } else {
                    SDL_SetWindowFullscreen(this->window, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
                }
            });

            shortcuts.subscribe("Cmd+Q"_shortcut, [this](const uio::shortcut &) {
                this->app.quit();
            });

            ui.hub.subscribe(mouse.on_move, [this](const uio::mouse_move_event & event) {
                if (controller.hover_move(space::point(event.mouse.position()))) {
                    SDL_SetCursor(hand_cursor.get());
                } else {
                    SDL_SetCursor(arrow_cursor.get());
                }
            });

            ui.hub.subscribe(mouse.on_press, [this](const uio::mouse_button_event & event) {
                if (event.button != uio::mouse_button::left) {
                    return;
                }

                controller.press(space::point(event.mouse.position()));
            });

            ui.hub.subscribe(mouse.on_release, [this](const uio::mouse_button_event & event) {
                if (event.button != uio::mouse_button::left) {
                    return;
                }

                controller.release(space::point(event.mouse.position()));
            });

            ui.hub.subscribe(mouse.on_wheel, [this](const uio::mouse_wheel_event & event) {
                controller.wheel(space::point(event.mouse.position()), space::point(event.delta));
            });

            ui.hub.subscribe(mouse.on_touch_start, [this](const uio::touch_event & event) {
                controller.touch_start(space::point(event.mouse.position()), event.touch_count);
            });

            ui.hub.subscribe(mouse.on_touch_end, [this](const uio::touch_event & event) {
                controller.touch_end(space::point(event.mouse.position()), event.touch_count);
            });

            ui.hub.subscribe(window.on_resize, [this](const sdl::window_size & logical_size, const math::int_size & physical_size) {
                spdlog::debug(physical_size);
                this->graphics.set_viewport(physical_size);
                this->ui.camera.set_viewport(logical_size);
            });
        }

        void update(std::chrono::milliseconds elapsed) {
            routines(elapsed);
            root.update({space::rect {window.logical_size()}, 0.0f});

            ui.update();
            ui.render();

            graphics.present();
        }
    };

    struct app_container
    {
        sdl::app & app;
        boost::asio::io_context io_context;
        flow::periodic_timer<std::chrono::milliseconds> timer;

        app_container(sdl::app & app) :
            app(app),
            timer(io_context, 16ms) {}

        int run(window_container & w) {
            timer.start([&w, this](auto elapsed) {
                if (!app.process_events()) {
                    io_context.stop();
                    return;
                }

                w.update(elapsed);
            });

            flow::run_main(io_context);

            return 0;
        }
    };

    template <class T>
    auto bind_memory_resource(std::pmr::memory_resource * resource) {
        using allocator_type = typename T::allocator_type;
        return boost::di::bind<allocator_type>().to(allocator_type{resource});
    }

    std::pmr::monotonic_buffer_resource common_memory_resource_upstream;
    std::pmr::unsynchronized_pool_resource common_memory_resource{&common_memory_resource_upstream};

    auto window_module() {
        namespace di = boost::di;

#if ASD_OPENGL_ES
        opengl::configuration opengl_config{opengl::profile::es, 3, 0, 0}; // opengl es
#else
        opengl::configuration opengl_config{opengl::profile::core, 3, 3, 0}; // desktop opengl
#endif

        return di::make_injector(
            di::bind<opengl::configuration>().to(std::move(opengl_config)),
            di::bind<sdl::window_title>().to("fonts"),
            di::bind<sdl::window_size>().to(sdl::window_size{640, 640}),

            bind_memory_resource<uio::mouse_input>(&common_memory_resource),
            bind_memory_resource<uio::keyboard_input>(&common_memory_resource),
            bind_memory_resource<sdl::app>(&common_memory_resource)
        );
    }

    auto root_module = boost::di::make_injector(sdl::module(), window_module());

    auto & a = root_module.create<app_container &>();
    auto & w = root_module.create<window_container &>();

    static entrance open([]() {
#ifdef ASD_DEBUG
        spdlog::set_level(spdlog::level::debug);
#endif

        try {
            return a.run(w);
        } catch (const std::exception & e) {
            spdlog::error(e.what());
            return -1;
        }
    });
}

//---------------------------------------------------------------------------
