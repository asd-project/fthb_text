//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <ui/systems/context.h>

#include <color/color.h>

#include <ui/widgets/widget.h>
#include <ui/model.h>
#include <ui/models/stacking_model.h>

#include <gfx/text_shape.h>
#include <boost/container/stable_vector.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        namespace s
        {
            using namespace ui::s;
        }

        enum class text_direction
        {
            automatic,
            ltr,
            rtl,
        };

        enum class text_alignment
        {
            start,
            center,
            end,
        };

        struct text_subrange
        {
#if defined(TYPOGRAPHY_USE_BOOST_TEXT)
            using text_type = gfx::text; // boost::text doesn't allow to break text into views
#else
            using text_type = gfx::text_view;
#endif
            text_subrange(const text_type & text, const gfx::font & font, const color::linear_rgb & color) :
                text(text), font(font), color(color) {}

            text_type text;
            gfx::font font;
            color::linear_rgb color;
            text_direction direction = text_direction::automatic;

            //
            span<gfx::glyph_slot> glyph_slots;
            space::rect area;
        };

        struct text_layer
        {
            std::vector<gfx::text> bidi_subranges;
            std::vector<text_subrange> subranges;
            float line_height = 1.25f;
            text_direction direction = text_direction::automatic;
            text_alignment alignment = text_alignment::start;

            //
            std::vector<gfx::glyph_slot> glyph_slots;
            space::rect area;
        };

        namespace widgets
        {
            class label : public ui::widgets::model_widget
            {
            public:
                struct def : ui::widgets::model_widget::def
                {
                    static constexpr meta::type<text_layer> text{};
                };

                static constexpr auto model_def = ui::stack(
                    def::geometry,
                    def::text
                );

                export_api(app)
                label(ecs::handle && model, const gfx::font & font) noexcept;

                label(label && p) noexcept = default;

                label & operator = (label && p) noexcept = default;

                const gfx::text & text() const noexcept {
                    return _text;
                }

                const gfx::font & font() const noexcept {
                    return _model[def::text].subranges[0].font;
                }

                const color::linear_rgb & color() const noexcept {
                    return _model[def::text].subranges[0].color;
                }

                text_direction direction() const noexcept {
                    return _model[def::text].direction;
                }

                text_alignment alignment() const noexcept {
                    return _model[def::text].alignment;
                }

                export_api(ui)
                void set_text(const gfx::text & text) noexcept;

                export_api(ui)
                void set_font(const gfx::font & font) noexcept;

                export_api(ui)
                void set_color(const color::linear_rgb & color) noexcept;

                export_api(ui)
                void set_direction(text_direction direction) noexcept;

                export_api(ui)
                void set_alignment(text_alignment alignment) noexcept;

            private:
                gfx::text _text;
            };
        }

        static constexpr struct
        {
            template <class... T, useif<
                symbol::matches<T...> (
                    s::context      [symbol::required][symbol::unique],
                    s::font         [symbol::required][symbol::unique],
                    s::text         [symbol::optional][symbol::unique],
                    s::direction    [symbol::optional][symbol::unique],
                    s::color        [symbol::optional][symbol::unique]
                )
            >>
            auto operator()(T &&... options) const noexcept {
                auto [named, _] = symbol::collect(std::forward<T>(options)...);
                widgets::label label(ui::make_model(named[s::context], widgets::label::model_def), named[s::font]);

                if constexpr (symbol::in<T...>(s::text)) {
                    label.set_text(named[s::text]);
                }

                if constexpr (symbol::in<T...>(s::direction)) {
                    label.set_direction(named[s::direction]);
                }

                if constexpr (symbol::in<T...>(s::color)) {
                    label.set_color(named[s::color]);
                }

                return label;
            }
        } label;
    }
}
