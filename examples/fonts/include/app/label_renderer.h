//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <bitset>

#include <math/range.h>
#include <ui/systems/context.h>

#include <gfx/text_shape.h>
#include <gfx/raster_font.h>

#include <app/label.h>

#include <gfx3d_templates/mesh_quad.h>
#include <algorithm/generators.h>

#include <boost/container/small_vector.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    namespace app
    {
        struct glyph_vertex
        {
            math::float_point pos;
            math::point<u16> uv;
            color::srgb8 color = {0, 0, 0, 0};
        };

        namespace vertex_attributes
        {
            struct integer_uv : gfx::vertex_attributes::attribute<u16, 2>
            {
                static constexpr char key[] = "t2";
                static constexpr char name[] = "texcoord";
            };

            struct packed_color : gfx::vertex_attributes::attribute<u8, 4, true>
            {
                static constexpr char key[] = "c4";
                static constexpr char name[] = "color";
            };
        }

        namespace uniforms
        {
            static constexpr auto Atlas = gfx::create_uniform_layout("Atlas",
                gfx::uniforms::element<space::vector>{"atlas_size"}
            );
        }

        constexpr math::point<u16> pack_uv(const math::float_point & pt) noexcept {
            return math::point<u16>(math::clamp(math::int_point(pt * std::numeric_limits<u16>::max()), {0}, {std::numeric_limits<u16>::max()}));
        }

        using glyph_quad = std::array<glyph_vertex, 4>;
        using glyph_vertex_layout = gfx::vertex_layouts::generator<gfx::vertex_attributes::p2, vertex_attributes::integer_uv, vertex_attributes::packed_color>;

        using glyph_quad_id = u16;
        using glyph_quad_ids = std::vector<glyph_quad_id>;
        constexpr auto glyph_buffer_size = 16 * 1024 / sizeof(glyph_quad);

        constexpr auto glyph_quad_buffer_indices = gfx::repeat_all<glyph_buffer_size>(gfx::quad_indices, [](auto index, auto round) {
            return static_cast<u16>(index + round * 4);
        });

        using glyph_slots = std::vector<gfx::glyph_slot>;

        template <class Gfx>
        struct glyph_mesh
        {
            static_assert(gfx::graphics<Gfx>);

            glyph_mesh(gfx::handle<gfx::mesh<Gfx>> && mesh) :
                mesh(std::move(mesh)),
                vertex_buffer(&this->mesh->add_vertex_buffer(glyph_vertex_layout::instance, quads))
            {
                this->mesh->set_indices(glyph_quad_buffer_indices);
                free_mask.set();
            }

            bool allocate_quads(glyph_quad_ids & indices, u32 count) {
                if (free_mask.count() < count) {
                    return false;
                }

                for (u16 i = 0; i < glyph_buffer_size && indices.size() < count; ++i) {
                    if (free_mask.test(i)) {
                        indices.push_back(i);
                        free_mask.reset(i);
                    }
                }

                if (dirty_range.empty()) {
                    dirty_range.assign({indices.front(), glyph_quad_id(indices.back() + 1)});
                } else {
                    dirty_range.include({indices.front(), glyph_quad_id(indices.back() + 1)});
                }

                return true;
            }

            void update_quads(const glyph_quad_ids & indices) {
                if (indices.empty()) {
                    return;
                }

                if (dirty_range.empty()) {
                    dirty_range.assign({indices.front(), glyph_quad_id(indices.back() + 1)});
                } else {
                    dirty_range.include({indices.front(), glyph_quad_id(indices.back() + 1)});
                }
            }

            void free_quads(const glyph_quad_ids & indices) {
                if (indices.empty()) {
                    return;
                }

                for (auto i : indices) {
                    quads[i] = {};
                    free_mask.set(i);
                }

                if (dirty_range.empty()) {
                    dirty_range.assign({indices.front(), glyph_quad_id(indices.back() + 1)});
                } else {
                    dirty_range.include({indices.front(), glyph_quad_id(indices.back() + 1)});
                }
            }

            void sync() {
                if (dirty_range.empty()) {
                    return;
                }

                vertex_buffer->update_region(dirty_range, span(quads.data() + dirty_range.min, quads.data() + dirty_range.max));
                dirty_range = {0, 0};
            }

            std::array<glyph_quad, glyph_buffer_size> quads;
            std::bitset<glyph_buffer_size> free_mask;

            gfx::handle<gfx::mesh<Gfx>> mesh;
            gfx::vertex_buffer<Gfx> * vertex_buffer;
            math::range<glyph_quad_id> dirty_range;
        };

        template <class Gfx>
        struct glyph_mesh_storage
        {
            static_assert(gfx::graphics<Gfx>);

            glyph_mesh_storage(gfx::mesh_registry<Gfx> * mesh_registry, gfx::uniform_scheme<Gfx> * atlas_size_uniform_scheme, const math::uint_size & atlas_size) :
                mesh_registry(mesh_registry),
                last_atlas_size(atlas_size),
                atlas_size_uniform(atlas_size_uniform_scheme->instance(space::vector{atlas_size}))
            {}

            glyph_mesh<Gfx> * allocate_quads(glyph_quad_ids & indices, u32 count) {
                for (auto & mesh : meshes) {
                    if (mesh.allocate_quads(indices, count)) {
                        return &mesh;
                    }
                }

                auto & mesh = meshes.emplace_back(mesh_registry->create({glyph_vertex_layout::instance}));
                mesh.allocate_quads(indices, count);

                return &mesh;
            }

            void sync(const math::uint_size & atlas_size) {
                if (last_atlas_size != atlas_size) {
                    atlas_size_uniform = space::vector(last_atlas_size = atlas_size);
                }

                for (auto & mesh : meshes) {
                    mesh.sync();
                }
            }

            void cleanup() {
                for (auto it = meshes.begin(); it != meshes.end();) {
                    glyph_mesh<Gfx> & mesh = *it;

                    if (mesh.free_mask.all()) {
                        it = meshes.erase(it);
                    } else {
                        ++it;
                    }
                }
            }

            gfx::mesh_registry<Gfx> * mesh_registry;
            boost::container::stable_vector<glyph_mesh<Gfx>> meshes;
            math::uint_size last_atlas_size;
            gfx::uniform<Gfx, space::vector> atlas_size_uniform;
        };

        inline space::point calculate_glyph_pos(const auto & slot_area, const auto & local_area, app::text_direction direction) {
            return {
                // direction == app::text_direction::rtl ? slot_area.right - local_area.right :
                slot_area.left + local_area.left,
                slot_area.top + local_area.top
            };
        }

        template <class Gfx>
        struct text_layer_fragment_gfx
        {
            static_assert(gfx::graphics<Gfx>);

            text_layer_fragment_gfx(glyph_mesh_storage<Gfx> * storage, app::text_subrange * subrange, asd::span<gfx::glyph_slot> slots, const space::size & point_scale) :
                subrange(subrange),
                slots(slots)
            {
                quads.reserve(this->slots.size());
                mesh = storage->allocate_quads(quads, static_cast<u32>(this->slots.size()));

                space::point pos {subrange->direction == app::text_direction::rtl ? subrange->area.right : subrange->area.left, subrange->area.top};
                auto color = color::srgb8(subrange->color);

                for (size_t i = 0; i < quads.size(); ++i) {
                    auto & slot = this->slots[i];

                    if (!slot.glyph) {
                        continue;
                    }

                    glyph_quad & quad = mesh->quads[quads[i]];

                    auto glyph_pos = calculate_glyph_pos(slot.area, slot.glyph->local_area, subrange->direction);
                    auto area = pos + space::rect(glyph_pos, slot.glyph->local_area.size()) / point_scale;
                    auto uv = slot.glyph->atlas_area;

                    quad[0].pos = {area.left, area.top};
                    quad[1].pos = {area.left, area.bottom};
                    quad[2].pos = {area.right, area.bottom};
                    quad[3].pos = {area.right, area.top};

                    quad[0].uv = {uv.left, uv.top};
                    quad[1].uv = {uv.left, uv.bottom};
                    quad[2].uv = {uv.right, uv.bottom};
                    quad[3].uv = {uv.right, uv.top};

                    quad[0].color = color;
                    quad[1].color = color;
                    quad[2].color = color;
                    quad[3].color = color;
                }
            }

            text_layer_fragment_gfx(text_layer_fragment_gfx &&) noexcept = default;

            ~text_layer_fragment_gfx() {
                mesh->free_quads(this->quads);
            }

            void update_pos(const space::size & point_scale) {
                mesh->update_quads(quads);

                space::point pos {subrange->direction == app::text_direction::rtl ? subrange->area.right : subrange->area.left, subrange->area.top};

                for (size_t i = 0; i < quads.size(); ++i) {
                    auto & slot = this->slots[i];

                    if (!slot.glyph) {
                        continue;
                    }

                    glyph_quad & quad = mesh->quads[quads[i]];

                    auto glyph_pos = calculate_glyph_pos(slot.area, slot.glyph->local_area, subrange->direction);
                    auto area = pos + space::rect(glyph_pos, slot.glyph->local_area.size()) / point_scale;

                    quad[0].pos = {area.left, area.top};
                    quad[1].pos = {area.left, area.bottom};
                    quad[2].pos = {area.right, area.bottom};
                    quad[3].pos = {area.right, area.top};
                }
            }

            glyph_mesh<Gfx> * mesh;
            app::text_subrange * subrange;
            asd::span<gfx::glyph_slot> slots;
            glyph_quad_ids quads;
            space::rect area;
        };

        template <class Gfx>
        struct text_layer_gfx
        {
            space::rect area;
            std::vector<text_layer_fragment_gfx<Gfx>> fragments;
        };

        template <class Graphics, class FontContext>
        class text_render_system
        {
        public:
            using graphics_type = Graphics;
            using font_context_type = FontContext;

            using quads = gfx::mesh_quad_factory<gfx::vertex_layouts::p3t2>;

            using render_data_type = app::text_layer_gfx<graphics_type>;

            static constexpr auto components = meta::type_v<app::text_layer>;
            static constexpr auto tags = ui::tags::stateful + ui::tags::reshaping + ui::tags::active + ui::tags::render;

            static constexpr auto & shader_code = ui::shaders["glyph"];

            text_render_system(
                entt::registry & registry,
                gfx::uniform_registry<graphics_type> & uniforms,
                gfx::mesh_registry<graphics_type> & meshes,
                gfx::shader_registry<graphics_type> & shaders,
                gfx::pipeline_registry<graphics_type> & pipelines,
                gfx::text_shaper<FontContext> & text_shaper,
                gfx::font_source<FontContext> & font_source,
                gfx::font_rasterizer<FontContext, Graphics> & font_rasterizer
            ) :
                _atlas_size_uniform_scheme(uniforms.scheme(app::uniforms::Atlas)),
                _shader(shaders.create({"glyph", shader_code})),
                _pipeline(pipelines.create({*_shader})),
                _mesh_registry(meshes),
                _shaper(text_shaper),
                _source(font_source),
                _rasterizer(font_rasterizer),
                _observer(registry, entt::collector.update<app::text_layer>().where<render_data_type>())
            {}

            text_render_system(text_render_system && s) = delete;
            text_render_system & operator = (text_render_system && s) = delete;

            template <class ModelDef>
            void populate(entt::registry & registry, entt::entity entity, ModelDef) {
                registry.emplace<render_data_type>(entity);
            }

            template <class ModelDef>
            void depopulate(entt::registry & registry, entt::entity entity, ModelDef) {
                registry.erase<render_data_type>(entity);

                for (auto & [_, mesh_storage] : _meshes) {
                    mesh_storage.cleanup();
                }
            }

            ui::geometry reshape(entt::registry & registry, entt::entity entity, const ui::geometry & geometry) {
                auto [r, m] = registry.get<render_data_type, app::text_layer>(entity);

                if (m.area == geometry.area) {
                    return geometry;
                }

                r.area = geometry.area;
                update_geometry(r, m);

                return {m.area, geometry.depth};
            }

            void update(entt::registry & registry) {
                _observer.each([&](const auto entity) {
                    auto [r, m] = registry.get<render_data_type, app::text_layer>(entity);
                    r.fragments.clear();
                    refill(r, m);
                });
            }

            void render(entt::registry &) {
                for (auto & [texture, mesh_storage] : _meshes) {
                    mesh_storage.sync(texture->size());
                }

                _pipeline->activate();

                for (auto & [texture, mesh_storage] : _meshes) {
                    texture->bind(0);
                    mesh_storage.atlas_size_uniform.activate();

                    for (auto & mesh : mesh_storage.meshes) {
                        mesh.mesh->render();
                    }
                }
            }

        private:
            struct fragment_desc
            {
                app::glyph_mesh_storage<graphics_type> * mesh_storage;
                size_t slots_begin;
                size_t slots_end;
            };

            struct subrange_desc
            {
                app::text_subrange * subrange;
                std::vector<fragment_desc> fragments;
                space::size size;
                size_t slots_begin;
                size_t slots_end;
            };

            void separate_fragments(std::vector<gfx::text_fragment> & fragments, const gfx::text_view & text, const asd::span<gfx::font_id> & fonts) noexcept {
                BOOST_ASSERT_MSG(!fonts.empty(), "There should be at least one font provided");

                auto add_fragment = [](auto & output, auto begin, auto end, gfx::font_id font_id) {
                    if (begin != end) {
                        output.push_back({gfx::text_view(begin.base(), end.base()), font_id});
                    }
                };

                auto font_id = fonts[0];
                auto word_begin = gfx::text_begin(text);
                auto text_end = gfx::text_end(text);

                for (auto it = word_begin; it != text_end; ++it) {
                    auto next_font_id = fonts[0]; // fallback to the first font
                    auto cp = gfx::text_code_point(it);

                    for (auto font_id : fonts) {
                        auto & face = _source.face(font_id);

                        if (face.has_character(cp)) {
                            next_font_id = font_id;
                            break;
                        }
                    }

                    if (font_id != next_font_id) {
                        add_fragment(fragments, std::exchange(word_begin, it), it, std::exchange(font_id, next_font_id));
                    }
                }

                add_fragment(fragments, word_begin, text_end, font_id);
            }

            void refill(render_data_type & data, app::text_layer & model) {
                model.glyph_slots.clear();

                for (auto & subrange : model.subranges) {
                    fill_slots(model.glyph_slots, subrange);
                }

                if (_subrange_desc_buffer.empty()) {
                    return;
                }

                if (model.direction == app::text_direction::automatic) {
                    model.direction = _subrange_desc_buffer[0].size.x >= 0 ? app::text_direction::ltr : app::text_direction::rtl;
                }

                auto point_scale = _source.point_scale();

                for (subrange_desc & desc : _subrange_desc_buffer) {
                    auto size = math::abs(desc.size) / point_scale;
                    desc.subrange->direction = desc.size.x < 0 ? app::text_direction::rtl : desc.size.x == 0 ? model.direction : app::text_direction::ltr;
                    desc.subrange->glyph_slots = asd::span<gfx::glyph_slot>{model.glyph_slots.data() + desc.slots_begin, model.glyph_slots.data() + desc.slots_end};
                    desc.subrange->area = space::rect(size);
                }

                for (subrange_desc & desc : _subrange_desc_buffer) {
                    for (fragment_desc & fragment_desc : desc.fragments) {
                        asd::span<gfx::glyph_slot> fragment_slots {model.glyph_slots.data() + fragment_desc.slots_begin, model.glyph_slots.data() + fragment_desc.slots_end};
                        data.fragments.emplace_back(fragment_desc.mesh_storage, desc.subrange, fragment_slots, point_scale);
                    }
                }

                _subrange_desc_buffer.clear();
            }

            void update_geometry(render_data_type & data, app::text_layer & model) {
                auto point_scale = _source.point_scale();

                if (model.direction != app::text_direction::rtl) {
                    space::point current_pos {data.area.left, data.area.top};
                    space::point edge_point = current_pos;

                    for (app::text_subrange & subrange : model.subranges) {
                        auto size = subrange.area.size();

                        if (current_pos.x + size.x > data.area.right && current_pos.x > data.area.left) {
                            current_pos.y = edge_point.y;
                            current_pos.x = data.area.left;
                        }

                        subrange.area = space::rect(current_pos, size);
                        current_pos.x = subrange.area.right;
                        edge_point = {math::max(edge_point.x, subrange.area.right), math::max(edge_point.y, current_pos.y + subrange.font.size * model.line_height)};
                    }

                    model.area = space::rect{data.area.left, data.area.top, edge_point.x, edge_point.y};
                } else {
                    space::point current_pos {data.area.right, data.area.top};
                    space::point edge_point = current_pos;

                    for (app::text_subrange & subrange : model.subranges) {
                        auto size = subrange.area.size();

                        if (current_pos.x - size.x < data.area.left && current_pos.x < data.area.right) {
                            current_pos.y = edge_point.y;
                            current_pos.x = data.area.right;
                        }

                        subrange.area = space::rect(current_pos - size, size);
                        current_pos.x = subrange.area.left;
                        edge_point = {math::min(edge_point.x, subrange.area.left), math::max(edge_point.y, current_pos.y + subrange.font.size * model.line_height)};
                    }

                    model.area = space::rect{edge_point.x, data.area.top, data.area.right, edge_point.y};
                }

                for (auto & fragment : data.fragments) {
                    fragment.update_pos(point_scale);
                }
            }

            void fill_slots(std::vector<gfx::glyph_slot> & slots, app::text_subrange & subrange) {
                auto id = subrange.font.id;

                if (!id.valid()) {
                    subrange.glyph_slots = {};
                    return;
                }

                subrange_desc & desc = _subrange_desc_buffer.emplace_back();
                desc.subrange = &subrange;
                desc.slots_begin = slots.size();
                separate_fragments(_fragments_buffer, subrange.text, asd::span(std::addressof(id), 1));

                for (auto & fragment : _fragments_buffer) {
                    auto offset = slots.size();
                    desc.size += _shaper.shape(slots, fragment, subrange.font.size, gfx::pixel_point(desc.size));
                    auto & atlas = _rasterizer.rasterize(slots, {subrange.font, gfx::raster_font_format::colored});

                    auto & glyph_mesh_storage = require_mesh(&atlas);

                    for (; offset < slots.size(); offset += app::glyph_buffer_size) {
                        fragment_desc & fragment_desc = desc.fragments.emplace_back();
                        fragment_desc.mesh_storage = &glyph_mesh_storage;
                        fragment_desc.slots_begin = offset;
                        fragment_desc.slots_end = std::min(offset + app::glyph_buffer_size, slots.size());
                    }
                }

                desc.slots_end = slots.size();
                _fragments_buffer.clear();
            }

            app::glyph_mesh_storage<graphics_type> & require_mesh(gfx::texture<graphics_type> * atlas) {
                auto it = _meshes.find(atlas);

                if (it != _meshes.end()) {
                    return it->second;
                }

                return _meshes.emplace(
                    std::piecewise_construct,
                    std::make_tuple(atlas),
                    std::make_tuple(&_mesh_registry, &_atlas_size_uniform_scheme, atlas->size())
                ).first->second;
            }

            gfx::uniform_scheme<graphics_type> & _atlas_size_uniform_scheme;
            gfx::handle<gfx::shader_program<graphics_type>> _shader;
            gfx::handle<gfx::pipeline<graphics_type>> _pipeline;
            gfx::mesh_registry<graphics_type> & _mesh_registry;
            gfx::text_shaper<font_context_type> & _shaper;
            gfx::font_source<font_context_type> & _source;
            gfx::font_rasterizer<font_context_type, graphics_type> & _rasterizer;
            entt::observer _observer;

            stable_map<gfx::texture<graphics_type> *, app::glyph_mesh_storage<graphics_type>> _meshes;

            std::vector<gfx::text_fragment> _fragments_buffer;
            std::vector<subrange_desc> _subrange_desc_buffer;
        };

        template <class Graphics, class FontContext>
        constexpr meta::type<text_render_system<Graphics, FontContext>> text_render_system_type{};
    }
}
