//---------------------------------------------------------------------------

#include <fthb_text/text_shape.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        static constexpr hb_tag_t kerning_feature = HB_TAG('k', 'e', 'r', 'n');
        static constexpr hb_tag_t standard_ligatures_feature = HB_TAG('l', 'i', 'g', 'a');
        static constexpr hb_tag_t contextual_ligatures_feature = HB_TAG('c', 'l', 'i', 'g');

        gfx::pixel_size text_shaper<fthb_text::context>::shape(std::vector<glyph_slot> & glyph_slots, const gfx::text_fragment & fragment, u32 size, gfx::pixel_point offset) noexcept {
            gfx::font font{fragment.font_id, size};

            auto it = _fonts.find(font);

            if (it == _fonts.end()) {
                it = _fonts.try_emplace(font, _source.size_face(font)).first;
            }

            constexpr std::array features = {
                hb_feature_t { kerning_feature, 1, 0, std::numeric_limits<unsigned int>::max() },
                hb_feature_t { standard_ligatures_feature, 1, 0, std::numeric_limits<unsigned int>::max() },
                hb_feature_t { contextual_ligatures_feature, 1, 0, std::numeric_limits<unsigned int>::max() }
            };

            auto & shaped_font = it->second;

            auto length = text_length(fragment.text);
            hb_buffer_add_utf8(_buffer, text_data(fragment.text), length, 0, length);
            hb_buffer_guess_segment_properties(_buffer);
            auto direction = hb_buffer_get_direction(_buffer);

            hb_shape(shaped_font.handle, _buffer, features.data(), static_cast<u32>(features.size()));
            // hb_shape(shaped_font.handle, _buffer, nullptr, 0);

            u32 count = hb_buffer_get_length(_buffer);
            hb_glyph_info_t * info = hb_buffer_get_glyph_infos(_buffer, nullptr);
            hb_glyph_position_t * positions = hb_buffer_get_glyph_positions(_buffer, nullptr);

            for (u32 i = 0; i < count; ++i) {
                gfx::glyph_slot & slot = glyph_slots.emplace_back();

                slot.id = gfx::glyph_id(info[i].codepoint);

                if (direction == HB_DIRECTION_RTL) {
                    slot.area = offset + math::rect(-positions[i].x_advance, -positions[i].y_advance, -positions[i].x_offset, -positions[i].y_offset) / gfx::px(64.f);
                    offset = slot.area.min;
                } else {
                    slot.area = offset + math::rect(positions[i].x_offset, positions[i].y_offset, positions[i].x_advance, positions[i].y_advance) / gfx::px(64.f);
                    offset = slot.area.max;
                }
            }

            hb_buffer_clear_contents(_buffer);

            return math::size(offset);
        }
    }
}

//---------------------------------------------------------------------------
