//---------------------------------------------------------------------------

#include <fthb_text/font_source.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace gfx
    {
        font_source<fthb_text::context>::font_source(fthb_text::context & context) :
            _context(context)
        {}

        gfx::font_id font_source<fthb_text::context>::load(const gfx::resource & resource) {
            auto & face = _faces.emplace_back();

            gfx::visit_resource(resource,
                [&](auto & memory) {
                    if (FT_New_Memory_Face(_context, reinterpret_cast<const FT_Byte *>(memory.data()), static_cast<FT_Long>(memory.size()), 0, &face.handle) != 0) {
                        BOOST_THROW_EXCEPTION(std::runtime_error("Couldn't create font face"));
                    }
                },
                [&](auto & path) {
                    if (FT_New_Face(_context, path.string().c_str(), 0, &face.handle) != 0) {
                        BOOST_THROW_EXCEPTION(std::runtime_error("Couldn't create font face"));
                    }
                });

            face.font.id = static_cast<gfx::font_id>(_faces.size() - 1);
            return face.font.id;
        }

        fthb_text::font_face & font_source<fthb_text::context>::face(gfx::font_id font_id) {
            return _faces[static_cast<size_t>(font_id)];
        }

        fthb_text::font_face & font_source<fthb_text::context>::size_face(gfx::font font) {
            auto & face = _faces[static_cast<size_t>(font.id)];

            if (face.font.size != font.size || face.dpi != _dpi) {
                face.font.size = font.size;
                face.dpi = _dpi;

                if (auto err = FT_Set_Char_Size(face.handle, 0, face.font.size * 64, _dpi.x, _dpi.y); err != 0) {
                    BOOST_THROW_EXCEPTION(std::runtime_error("Couldn't set font char size"));
                }
            }

            return face;
        }
    }
}

//---------------------------------------------------------------------------
