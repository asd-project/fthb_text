# `asd.fthb_text` (FreeType + HarfBuzz binding for `asd.text`)

![Text layout example](doc/fonts_example.png)

Custom UI component (based on `asd.ui`) with text is in [examples/fonts/include/app/label.h](./examples/fonts/include/app/label.h) / [examples/fonts/src/app/label.cpp](./examples/fonts/src/app/label.cpp)  

See [examples/fonts/include/app/label_renderer.h](./examples/fonts/include/app/label_renderer.h) for the custom text renderer example
