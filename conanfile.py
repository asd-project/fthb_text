import os

from conans import ConanFile, CMake

project_name = "fthb_text"


class FTHBTextConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Components for font rendering and text shaping based on FreeType and HarfBuzz"
    settings = "os", "compiler", "build_type", "arch"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.typography/0.0.1@asd/testing",
    )
    default_options = {
        "harfbuzz:with_glib": False,
        "freetype:with_png": False,
        "freetype:with_zlib": False
    }

    def requirements(self):
        if self.settings.os != "Emscripten":
            self.requires("freetype/[>=2.10.4]")
            self.requires("harfbuzz/[>=2.8.1]")

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*.hpp", dst="include", src="include")
        self.copy("*.ipp", dst="include", src="include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = [project_name]
